from paraview.simple import *
import numpy as np
try:
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_reader.py", ns=globals())
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_filters.py", ns=globals())
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_projection.py", ns=globals())
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_annotation.py", ns=globals())
except Exception as e:
    print(e)

eamdata = EAMDataReader(registrationName='eamdata', 
        ConnectivityFile='/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/TEMPEST_ne30pg2.scrip.renamed.nc',
        DataFile='/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/aerosol_F2010.eam.h0.2014-12.nc')
eamdata.a2DVariables = []
eamdata.a3DMiddleLayerVariables = ['T', 'U']
eamdata.a3DMiddleLayerVariables = ['T', 'U']


tk = GetTimeKeeper()
tk.Time = tk.TimestepValues[0]

eamproj = EAMProject(registrationName='eamproj', Input=OutputPort(eamdata, 0))
eamproj.Projection = 1
eamproj.Translate  = 1
eamproj.UpdatePipeline()

Show(eamproj)
Render()
Interact()
SaveScreenshot('screenshot.png')
