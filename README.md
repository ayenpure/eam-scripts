# PEASCAL ParaView Plugins

This repository contains the ParaView/VTK utilities developed for the [PEASCAL](https://github.com/PAESCAL-SciDAC5)
(Physical, Accurate, and Efficient atmosphere and surface coupling across SCALes) project,
a SciDAC partnership between BER and ASCR.
The focus of this project is to better the Environment Atmospheric Model (EAM), which is a component
of a larger Earth System Model by the DoE called [E3SM](https://e3sm.org/) (Energy Exascale Earth System Model).  
These plugins contain the readers, filters, and other utilities necessary to visualize and process
EAM data using ParaView.

### Overview

EAM data consists of `netCDF` files that contain lots of variables.
Currently the the coordinates and topology (connectivity) of the data and the varaiables are stored
in separate files, necessitating a custom reader for the data.
Further, various filters are developed to enable manipulation of the EAM data to present it in a 
convenient, easy to comprehend manner.
The reader and filters are separated in three differnt files.

  1. EAM Reader (`eam_reader.py`) : Contains the reader for the `netCDF file`.
  2. EAM Filters (`eam_filters.py`) : Contains filters to manipulate the data from the reader.
  3. EAM Projections (`eam_projection.py`) : Contains filters for map projections and visualization of data on the globe.

### Usage

To get setup quickly it is recommended to use a new python environment.
For convenience, below is a listing to setup the environment using Conda.

```
conda create --name eamviz python=3.10.0 ipython
conda activate eamviz
conda install -c conda-forge paraview=5.11.1 netCDF4 pyproj numpy
```

The above setup should get Linux users up and running, however, 
MacOS users might need to also include the package `libcxx=16.0.3` 

Following successful installation of all the packages,
it is recommended that users use `pvpython` which is ParaView's wrapper around `python` to execute scripts
that use any of the above plugins. However, to use them within `ParaView` they'll simply need to import/load the plugins within ParaView.
The following code snippet demonstate the use of the plugins in a python script.

```python
from paraview.simple import *
import numpy as np


"""
Load all the plugins in ParaView programatically.
"""
try:
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_reader.py", ns=globals())
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_filters.py", ns=globals())
    LoadPlugin("/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/eam_projection.py", ns=globals())
except Exception as e:
    print(e)

"""
Use the data reader to read netCDF data by specifying the data and connectivity files
"""
eamdata = EAMDataReader(registrationName='eamdata', 
        ConnectivityFile='/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/TEMPEST_ne30pg2.scrip.renamed.nc',
        DataFile='/home/local/KHQ/abhi.yenpure/repositories/eam/scripts/aerosol_F2010.eam.h0.2014-12.nc')
eamdata.a2DVariables = []
eamdata.a3DMiddleLayerVariables = ['T', 'U']

tk = GetTimeKeeper()
tk.Time = tk.TimestepValues[0]

"""
Represent the data using the Robinson projection
"""
eamproj = EAMProject(registrationName='eamproj', Input=OutputPort(eamdata, 0))
eamproj.Projection = "Robinson"
eamproj.Translate  = 1
eamproj.UpdatePipeline()


"""
Have ParaView show the data, and save a screenshot
"""
Show(eamproj)
Render()
Interact()
SaveScreenshot('screenshot.png')
```
For more advanced operations, refer to [ParaView's scripting guide](https://docs.paraview.org/en/latest/Tutorials/SelfDirectedTutorial/batchPythonScripting.html).

### Example Visualization

Following is an example visualization generated using the ParaView plugins and other filters.
It shows a `data curtain`, a vertical slice of data along the 0&deg; Longitude within the arctic circle for 
three different varaibles.

![Data Curtains](images/NewLayout.png)
