import math
from vtkmodules.vtkFiltersCore import vtkAppendFilter
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase

from paraview.detail.pythonalgorithm import (
    smproxy,
    smproperty,
    smdomain,
    smhint
)

from paraview.simple import (
    AppendDatasets,
    FindViewOrCreate,
    Line,
    Show,
    Text,
)

def ProcessPoint(point, max, radius, scale):
    #theta = math.radians(point[0] - 180.)
    #phi   = math.radians(point[1])
    #rho   = 1.0
    theta   = point[0]
    phi     = 90 - point[1]
    rho     = (max - point[2]) * scale + radius if not point[2] == 0 else radius
    x = rho * math.sin(math.radians(phi)) * math.cos(math.radians(theta))
    y = rho * math.sin(math.radians(phi)) * math.sin(math.radians(theta))
    z = rho * math.cos(math.radians(phi))
    return [x, y, z]


@smproxy.source(name="EAMGridLines")
class EAMGridLines(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
            nInputPorts=0,
            nOutputPorts=1,
            outputType='vtkUnstructuredGrid')
        self.longitude = 0

    def RequestInformation(self, request, inInfo, outInfo):
        return super().RequestInformation(request, inInfo, outInfo)

    def RequestUpdateExtent(self, request, inInfo, outInfo):
        return super().RequestUpdateExtent(request, inInfo, outInfo)

    def RequestData(self, request, inInfo, outInfo):
        line1 = Line(registrationName='Line1')
        line1.Point1 = [360.0, 66, 600.0]
        line1.Point2 = [360.0, 90.0, 600.0]
        line1.Resolution = 100
        line2 = Line(registrationName='Line2')
        line2.Point1 = [360.0, 66, 800.0]
        line2.Point2 = [360.0, 90.0, 800.0]
        line2.Resolution = 100
        line3 = Line(registrationName='Line3')
        line3.Point1 = [360.0, 66, 1000.0]
        line3.Point2 = [360.0, 90.0, 1000.0]
        line3.Resolution = 100
        line4 = Line(registrationName='Line4')
        line4.Point1 = [360.0, 66.5, 570.]
        line4.Point2 = [360.0, 66.5, 1000.]
        line4.Resolution = 100
        line5 = Line(registrationName='Line5')
        line5.Point1 = [360.0, 90.0, 570.]
        line5.Point2 = [360.0, 90.0, 1000.]
        line5.Resolution = 100

        appendDatasets1 = AppendDatasets(registrationName='AppendDatasets1', Input=[line1, line2, line3, line4, line5])
        appendDatasets1.UpdatePipeline()

        return 1

def SetTextProperty(input, pos, *views):
    text       = Text(registrationName=input)
    text.Text  = input
    for view in views:
        display = Show(text, view, 'TextSourceRepresentation')
        display.WindowLocation = 'Any Location'
        display.TextPropMode = 'Billboard 3D Text'
        display.BillboardPosition = pos
        display.Bold = 1
        display.FontSize = 22
        display.Italic = 1
        display.Shadow = 1
    pass

@smproxy.source(name="EAMAnnotation")
class EAMAnnotation(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
            nInputPorts=0,
            nOutputPorts=1)
        self.radius = 2000
        self.scale  = 1.6

    def RequestInformation(self, request, inInfo, outInfo):
        return super().RequestInformation(request, inInfo, outInfo)

    def RequestUpdateExtent(self, request, inInfo, outInfo):
        return super().RequestUpdateExtent(request, inInfo, outInfo)

    def RequestData(self, request, inInfo, outInfo):

        renderView1 = FindViewOrCreate('RenderView1', viewtype='RenderView')
        renderView2 = FindViewOrCreate('RenderView2', viewtype='RenderView')
        renderView3 = FindViewOrCreate('RenderView3', viewtype='RenderView')

        pos1 = ProcessPoint([360.0, 66., 630.0], 1000, self.radius, 1.6)
        SetTextProperty("600 hPa", pos1, renderView1, renderView2, renderView3)

        pos2 = ProcessPoint([360.0, 66., 830.0], 1000, self.radius, 1.6)
        SetTextProperty("800 hPa", pos2, renderView1, renderView2, renderView3)

        pos3 = ProcessPoint([360.0, 66., 1030.0], 1000, self.radius, 1.6)
        SetTextProperty("1000 hPa", pos3, renderView1, renderView2, renderView3)

        pos4 = ProcessPoint([360.0, 67.5, 580.0], 1000, self.radius, 1.6)
        SetTextProperty("66.5 N", pos4, renderView1, renderView2, renderView3)

        pos5 = ProcessPoint([360.0, 90., 580.0], 1000, self.radius, 1.6)
        SetTextProperty("90 N", pos5, renderView1, renderView2, renderView3)

        return 1
