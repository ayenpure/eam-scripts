import netCDF4
import numpy as np
import sys
sys.path.append(r'/home/local/KHQ/abhi.yenpure/tools/ParaView-5.11.0-MPI-Linux-Python3.9-x86_64/lib/python3.9/site-packages')
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.vtkCommonCore import vtkPoints
from vtkmodules.vtkCommonDataModel import vtkUnstructuredGrid, vtkCellArray, vtkPolyLine, vtkPolyData
from vtkmodules.vtkIOLegacy import vtkUnstructuredGridWriter, vtkPolyDataWriter
from vtkmodules.util import vtkConstants
import shapefile

sf = shapefile.Reader("/home/local/KHQ/abhi.yenpure/repositories/eam/world/World_Continents.shp")
points  = []
parts   = []
shapes = sf.shapes()
for shape in shapes:
    print("New Shape")
    points.append(shape.points)
    print(type(points))
    parts.append(shape.parts)
    print(type(parts))
print(len(points))
print(len(parts))
pointarr = vtkPoints()
offsets = []
for arr in points:
    offsets.append(len(arr))
    for point in arr:
        pointarr.InsertNextPoint(point[0], point[1], 0.0)
cells = vtkCellArray()
print(len(offsets))
k = 0
for (arr, offset) in zip(parts, offsets):
    print(arr)
    arr.append(offset)
    for i in range(len(arr) - 1):
        start   = k + arr[i]
        end     = k + arr[i+1]
        print(start, end)
        polyLine = vtkPolyLine()
        polyLine.GetPointIds().SetNumberOfIds(end - start)
        for j in range(start, end):
            polyLine.GetPointIds().SetId(j - start, j)
        cells.InsertNextCell(polyLine)
    k = k + offset
polydata = vtkPolyData()
polydata.SetPoints(pointarr)
polydata.SetLines(cells)

w = vtkPolyDataWriter()
w.SetInputData(polydata)
w.SetFileName("world.vtk")
w.Write()
